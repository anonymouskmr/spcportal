.. spcportal documentation master file, created by
   sphinx-quickstart on Sun Sep 26 12:25:13 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to spcportal's documentation!
=====================================

SPCPortal is a free and open source web platform that helps you manage students details, campus recruitement and more...

The portal is being designed for the department, faculties and students, to keep them updated about the internship/placement statistics as well as the upcoming opportunities. The portal will help students to efficiently manage their time and will provide them with an all-in-one hub for internship/placement activities. This portal will help the students and placement department to efficiently manage applications for companies and keep track of offers made to the students.


.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/I8NV3oDUxRY?start=13" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>




installation
 - dev 
 - docker image 
 - production 

Tips and tricks

contributing
___________________________-


.. toctree::
   installation

.. toctree::
   :maxdepth: 2
   :caption: Contents:



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
